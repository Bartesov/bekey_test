$(document).ready(function() {

	var movie = document.getElementById("movie");

	movie.ontimeupdate = function() {
		if(movie.currentTime > 23.9 && movie.currentTime < 27.1){
			$('.gift').css('display', 'block');
		} else {
			$('.gift').css('display', 'none');
		}
	};

	$('.gift').click(function (e) {
		e.preventDefault();
        $('#movie')[0].pause();
		$('.pop-up').animate({
			left: "0"
		}, 700);
	});

	$('.close').click(function() {
        $('#movie')[0].play();
        $('.pop-up').animate({
			left: "-100%"
		}, 700);
	});

});